FROM debian
WORKDIR /code

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		make \
		gcc-arm-none-eabi \
		libnewlib-arm-none-eabi \
		binutils-arm-none-eabi \
	; \
	rm -rf /var/lib/apt/lists/*
